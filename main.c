/******************************************************************************************************************
    Auteur : Benjamin Venezia
    Date   : 29.12.2020
    Objectif : factorisation de code en .h et .c, fonctions.

******************************************************************************************************************/

/*****Ajout du header contenant les biblioth�ques et les prototypes ******/
#include "implementation.h"

int main()
{
    struct main body;              //structure inutile, juste pour pr�ciser que la cmd dans le switch intervient dans le body...
    unsigned int size_array;       //taille du tableau
    int array[MAX_SIZE_ARRAY+1] ;  //initialisation de array avec la macro MAX_SIZE_ARRAY
    int command;                   //command d�finit de quelle mani�re est ins�r�e la valeur � l'aide de la fonction (insertion element)
                                   // en effet, selon si le tableau est tri� ascendant ou descendant on devra adapter.
    bool program_end = false;      // sert � boucler dans le switch menu principal.
    bool restart = false;
    bool sorted;
    bool insertion;

    /******** message de bienvenue *********/
    welcome();

    /******** si reset on revient ici ********/
    reset:
    if(restart)
        reset();

    /*********************** saisie taille du tableau ********************************/
    size_array = enter_size_array();

    /*********************** remplissage du tableau par l'utilisateur ****************/
    fill_array(size_array, array);

    program_end = true;
    sorted = false;
    insertion = false;
    restart = true;

    do
    {
    /*********************** 1) Affichage du menu, l'utilisateur choisit ce qu'il veut faire ****************/
        body.cmd = main_menu();

    /*********************** Selon choix � 1) on fait l'action stock�e dans body.cmd ****************/
        switch (body.cmd)
        {
        case SORT_FUNCTIONS:
            body.cmd = sort_method_choice();
            sort_function(body.cmd, size_array, array, command, &sorted, insertion); //RENVOI VERS LE SOUS-MENU DES FONCTIONS DE TRI
            break;

        case SEARCH_FUNCTIONS:
            body.cmd = search_method_choice(); //RENVOI VERS LE SOUS-MENU DES FONCTIONS DE RECHERCHE
            search_functions(body.cmd, size_array, array);
            break;

        case INSERTION:
            insertion_element(size_array, array, command, &insertion); //RENVOI VERS LA FONCTION insertion_element
            break;

        case PRINT_ARRAY:
            if(insertion)
                print_array(size_array+1, array, AFTER_INSERT); //si insertion est pass� vrai, taille + message adapt�s.
            else if (sorted)
                print_array(size_array, array, SORTED_ARRAY);   // si sorted est pass� vrai, message adapt�.
            else
                print_array(size_array, array, NORMAL);         //sinon affichage normal, message adapt�.
            break;

        case RESET:
            goto reset; //on repart vers l'ancre et on recommence � partir de la taille du tableau.
            break;

        case STOP:
            program_end = false;
            break;

        default:
            printf("\nUnknown command");
        }
    } while(program_end);

    goodbye();

    return 0;
}








