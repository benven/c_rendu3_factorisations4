/******************************************************************************************************************
    Auteur : Benjamin Venezia
    Date   : 30.10.2020
    langue : rendu r�dig� en anglais, commentaires en fran�ais

    Cette version est am�lior�e. Le programme n'est plus proc�dural (taille, remplir, tri, recherche...) mais laisse le choix
    � l'utilisateur d'effectuer les actions qu'il d�sire.

******************************************************************************************************************/


#include "implementation.h"

/******************************  FUNCTION print array ****************************/
void print_array(int size_array, int array[], int specific_print)
{
    int i;

    if(specific_print == NORMAL){
    printf("=======================================================================\n");
    printf("Initial array : ");
        for(i = 0; i < size_array; i++) {
            printf(" %d ", array[i]);
        }
        printf("\n=======================================================================\n");
    }
    else if(specific_print == SORTED_ARRAY){
        printf("=======================================================================\n");
        printf(" sorted array : ");
        for(i = 0; i < size_array; i++) {
            printf(" %d ", array[i]);
        }
        printf("\n=======================================================================\n");
    } else if (specific_print == AFTER_INSERT){
         printf("======================================================================\n");
        printf(" after insertion: ");
        for(i = 0; i < size_array; i++) {
            printf(" %d ", array[i]);
        }
        printf("\n=====================================================================\n");
    }
}

/****************************** FUNCTION  read the value ****************************/
int enter_the_value_to_find_in_the_array()
{
    int VAL;
    do
    {
        printf("\nEnter the value to research : ");
        scanf("%d", &VAL);
        if(VAL < MIN_VAL || VAL > MAX_VAL)
            printf("Warning, enter a correct number between [%d - %d]\n", MIN_VAL, MAX_VAL);
    }
    while(VAL < MIN_VAL || VAL > MAX_VAL);
    return VAL;
}
/****************************** FUNCTION fill the array ****************************/
void fill_array(int size_array, int array[])
{
    int i;
    printf("\n\n_______________________________________________________________________\n\n");
    printf("                               Fill the array");
    printf("\n_______________________________________________________________________\n\n");
    for(i = 0; i < size_array; i++)
    {
        do
        {
            printf("Enter value in the position [%d] of the array : ", i + 1);
            scanf("%d", &array[i]);

            if(array[i] < MIN_VAL || array[i] > MAX_VAL)
                printf("Warning, enter a correct number between [%d - %d]\n", MIN_VAL, MAX_VAL);
        }
        while(array[i] < MIN_VAL || array[i] > MAX_VAL);
    }
     printf("_______________________________________________________________________\n\n\n");
}

/****************************** FUNCTION  read the value ****************************/
int enter_size_array()
{
    int size_array;
    do
    {
        printf("Define the size of the array [1 - %d] : ", MAX_SIZE_ARRAY);
        scanf("%d", &size_array);
        if(size_array <= 0 || size_array > MAX_SIZE_ARRAY)
            printf("Warning, enter a size array between [1 - %d]\n", MAX_SIZE_ARRAY);

    }
    while(size_array <= 0 || size_array > MAX_SIZE_ARRAY);

    return size_array;
}

/****************************** FUNCTION  insertion element ****************************/
int insertion_element(int size_array, int array[], int command, bool * insertion)
{
    int temp, i;

        printf("\n\nInsert an element : ");
        scanf("%d", &array[size_array]);

    size_array++;

    for (i = size_array-1; i > 0; i--)
    {
        if (command == 1 && array[i] < array[i-1])   //Si command == 1, insertion sort
        {
            temp = array[i - 1];
            array[i - 1] = array[i];
            array[i] = temp;
        }
        else if(command == 2 && array[i] > array[i-1])     // si command == 2, bubble sort
        {
            temp = array[i - 1];
            array[i - 1] = array[i];
            array[i] = temp;
        }
    }
    *insertion = true;

    printf("\n** Element inserted! **\n");
}

/************************* Message de bienvenue **********************************/
void welcome(){
    printf("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-\n");
    printf("---------------------------------------------------------------------------------------------\n");
    printf("This program fill an array, add a value in, sort, search via associates functions.\n");
    printf("---------------------------------------------------------------------------------------------\n");
    printf("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-\n\n");
}

/************************* Message de bienvenue **********************************/
int main_menu(){
    int choice;

    printf("\n-----------------------------------------------------------------------");
    printf("\n> %d.Sorting functions", SORT_FUNCTIONS);
    printf("\n> %d.Searching functions", SEARCH_FUNCTIONS);
    printf("\n> %d.Insert an element", INSERTION);
    printf("\n> %d.print array", PRINT_ARRAY);
    printf("\n> %d.reset", RESET);
    printf("\n> %d.EXIT", STOP);
    printf("\n-----------------------------------------------------------------------\n");
    printf("\nDo a choice :");
    scanf("%d", &choice);
    return choice;
}

/************************* menu de choix de la methode de tri **********************************/
int sort_method_choice() {
    int choice;
    do
    {
        printf("choose a Sort algorithm : \n");
        printf("-----------------------------------------------------------------------\n");
        printf("%d. Bubble sort. \n", BUBBLE_SORT);
        printf("%d. Insertion sort   \n", INSERTION_SORT);
        printf("-----------------------------------------------------------------------\n\n");
        printf("You're choice : ");
        scanf("%d",&choice);
    }
    while (choice < COMMAND_MIN || choice> COMMAND_MAX);
    return choice;
}

/************************* menu de choix de la methode de recherche **********************************/
int search_method_choice() {
    int choice;

    do
    {
        printf("\nPlease do a choice :      \n");
        printf("---------------------------------------------------------------\n");
        printf("%d. linear search            \n", LINEAR_SEARCH);
        printf("%d. linear sentinel search   \n", LINEAR_SENTINEL_SEARCH);
        printf("%d. Linear recursive search  \n", RECURSIVE_SEARCH);
        printf("%d. dichotomic search        \n", DICHOTOMIC_SEARCH);
        printf("---------------------------------------------------------------\n\n");
        printf("You're choice : ");
        scanf("%d", &choice);
    }
    while(choice < COMMAND_MIN || choice > COMMAND_MAX);

    return choice;
}
/***** affiche un message d'aurevoir *****/
void goodbye(){
printf("\t ___ _   _ _  \n\
        (  ,) ( \/(  _) \n\
    \t ),\ \  / / |  _) \n\
        (___/(_/  (___) \n\
      ");
}
/***** affiche le restart *****/
void reset(){
printf("\nYou reset the program!\n");
}



