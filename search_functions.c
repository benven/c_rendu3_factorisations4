/******************************************************************************************************************
    Auteur : Benjamin Venezia
    Date   : 30.10.2020
    langue : rendu r�dig� en anglais, commentaires en fran�ais

    Ce fichier .c contient toutes les fonctions de recherche

******************************************************************************************************************/

#include "implementation.h"

/******************* Ce switch permet d'orienter le choix vers une fonction sp�cifique. Le choix se d�finit dans le main **********************/

search_functions(int choice, int size_array, int array[]){

  switch(choice){
            int VAL;

            case LINEAR_SEARCH:
                 VAL = enter_the_value_to_find_in_the_array();
                 linear_search(size_array, array, VAL);
                break;
            case LINEAR_SENTINEL_SEARCH:
                 VAL = enter_the_value_to_find_in_the_array();
                 linear_sentinel_search(size_array, array, VAL);
                break;
            case RECURSIVE_SEARCH:
                 VAL = enter_the_value_to_find_in_the_array();
                 recursive_linear_search(size_array, array, 0, VAL);
                break;
            case DICHOTOMIC_SEARCH:
                 VAL = enter_the_value_to_find_in_the_array();
                 dichotomic_search(size_array, array, VAL);
                break;
            default:
                 VAL = enter_the_value_to_find_in_the_array();
                 printf("\n[Incorrect value] We choose choice : %d for you.\n", LINEAR_SEARCH);
                 linear_search(size_array, array, VAL);
                break;
        }
}


/******************************  FUNCTION  linear sentinel search  ****************************/
/***** Explications : Cette variante permet d'optimiser la recherche lin�aire en ne faisant qu'une seule comparaison
                      � la place de deux. Ainsi � la place de (TANT QUE I < N) et (SI I == VAL), nous aurons que (TANT QUE i != VAL)
                      � chaque it�ration.
                      On place l'�l�ment cherch� (VAL) en derni�re position du tableau
                      et on part du principe que m�me si la valeur ne s'y trouve pas, la fonction de recherche
                      butera bien � cet endroit. En v�rification il faut donc que i soit inf�rieur � n et on
                      v�rifie si la derni�re valeur, qu'on a prit soin de sauvegard�, ne correspond pas.

*/
void linear_sentinel_search(int size_array, int array[], int VAL)
{
    int i;
    i = 0;
    int dernier = array[size_array-1];

    array[size_array-1] = VAL;

    while(array[i] != VAL)
        i++;

    array[size_array-1] = dernier;

    if(i < size_array-1 || array[size_array-1] == VAL)
        printf("The linear sentinel search find the value %d at the position : %d of the array", VAL, i);
    else
        printf("Not find");
}

/******************************  FUNCTION dichotomic search  ****************************/
void dichotomic_search(int size_array, int array[], int VAL)
{
    int p = 1;                    //Borne gauche du tableau ex.  1 2 [3(p) 4 5 6 7](size_array)
    int q;                        // q est calcul� en faisant la moyenne entre la borne gauche (p) et la borne droite.
    //dans le cas au dessus. p vaut 3 size array 7 donc q vaudra 7/3 = 3 ->  1[0] 2  3  4  5  6  7 (q vaudra [4])
    int POS = -1;                 //POS vaut -1 si pas de match. sinon il prend la valeur de la cellule du tableau.

    while(p <= size_array)
    {
        q = (p + size_array) / 2;

        if (array[q] == VAL)
        {
            POS = q;                //Es-t-on tomb� sur la valeur recherch�e?
            break;
        }
        else if(array[q] > VAL)  //Si non, on regarde � la moiti� du tableau/sous-tableau si la valeur est plus grande ou plus petite.
            size_array = q - 1;     //Si la valeur de q est plus grande, on red�finit la BORNE DROITE pour la mettre � gauche de la valeur.
        else if(array[q] < VAL)
            p = q + 1;              //Si la valeur en array[q] est plus petite que val, on rapproche la BORNE GAUCHE.
    }
    if(POS == -1)
        printf("Sorry, value don't find. ");
    else
        printf("The value %d is found in the position %d",VAL, POS);
}
/******************************   FUNCTION  linear search  ****************************/
/*** Description : On parcours tous les �l�ments  */
void linear_search (int size_array, int array[], int VAL)
{
    int POS;
    int i;
    for (i = 0, POS = -1; i <= size_array; i++)
    {
        if (array[i] == VAL)
            POS = i;
    }
    if(POS == -1) printf("Sorry, value don't find. ");
    else printf("The value %d is found in the position %d",VAL, POS);
}
/******************************   FUNCTION  linear recursive search  ****************************/
/*** Explications : � la place de g�rer notre recherche avec une boucle, nous la g�rons par appel succesif
                    de la fonction. Si i est plus grand ou �gal � la taille du tableau, nous n'avons rien trouv�
                    et pouvons retourner un message le pr�cisant.
                    Si array[i] match avec la valeur, on effectue un return qui mettra fin � la r�cursivit�.
                    Sinon, nous sommes toujours en recherche, et dans ce cas, on retourne la fonction en prenant
                    soin de pr�ciser que i s'incr�mente en param�tre.
                    Attention : la r�cursivit� peut amener � des erreurs de stack overflow, si la liste d'appel
                    devient trop grand car la liste � parcourir est trop longue, le programme s'arr�tera sur une erreur.
                    Dans ce cas, nous pr�f�rerons la boucle qui est moins gourmande.
 */
int recursive_linear_search (int size_array, int array[], int i,  int VAL)
{
    if(i >= size_array+1)
        return printf("The linear recursive function doesn't find any value.");
    if(array[i] == VAL)
        return printf("The value %d is found at the position : [%d] of the array.",VAL, i);

    return recursive_linear_search(size_array, array, i += 1, VAL);
}
