/******************************************************************************************************************
    Auteur : Benjamin Venezia
    Date   : 30.10.2020
    langue : rendu r�dig� en anglais, commentaires en fran�ais

    Ce fichier .c contient toutes les fonctions de tri

******************************************************************************************************************/

#include "implementation.h"

/******************************  Sous-menu choix fonction tri   ****************************/
sort_function(int choice, int size_array, int array[], int command, bool *sorted, bool is_inserted){
 switch (choice)
        {
            case BUBBLE_SORT:
                *sorted = true;
                bubble_sort(size_array, array, &command);
                printf("** Array sorted! **");
                break;

            case INSERTION_SORT:
                *sorted = true;
                insertion_sort(size_array, array, &command, is_inserted);
                printf("** Array sorted! **");
                break;

            default:
                *sorted = true;
                printf("\n[Incorrect value] We choose choice : %d for you.\n", BUBBLE_SORT);
                bubble_sort(size_array, array, &command);
                printf("** Array sorted! **");
                break;
        }
}

/******************************  FUNCTION  bubble sort ****************************/
void bubble_sort(int size_array, int array[], int *command)
{
    int temp;
    int i, j;

    for (i = 0; i < size_array-1; i++)
    {
        for (j = 0; j < size_array - i - 1; j++)   // On d�duit les valeurs d�j� tri�es.
        {
            if(array[j] > array[j+1])    /*Si array[j] > array[j+1] on les permute. Si non, on passe � la case suivante.*/
            {
                temp = array[j+1];
                array[j+1] = array[j];
                array[j] = temp;
            }
        }
    }

    *command = 1; //on doit d�finir la mani�re dont est tri� (asc ou desc) pour l'insertion
}
/******************************  FUNCTION  insertion sort ****************************/
void insertion_sort(int size_array, int array[],int *command, bool insertion)
{
    int index_max;
    int temp;
    int i,j;

    if(insertion)
        size_array++;

    for (i = 0; i < size_array ; i++)
    {
        for (j = i + 1, index_max = i; j < size_array  ; j++)
        {
            if (array[index_max] < array[j])
                index_max = j;
        }
        if(index_max != i)
        {
            temp = array[index_max];
            array[index_max] = array[i];
            array[i] = temp;
        }
    }

    *command = 2; //on doit d�finir la mani�re dont est tri� (asc ou desc) pour l'insertion
}



