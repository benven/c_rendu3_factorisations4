/******************************************************************************************************************
    Auteur : Benjamin Venezia
    Date   : 30.10.2020
    langue : rendu r�dig� en anglais, commentaires en fran�ais

    Ce fichier .h contient les prototypes des fonctions, rassemble les diff�rentes biblioth�ques, les define.


******************************************************************************************************************/

/*#ifndef implementation
#endif
#define implementation*/

/*******Biblioth�ques********/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>

/*******define SWITCH PRINCIPAL********/

#define SORT_FUNCTIONS 1
#define SEARCH_FUNCTIONS 2
#define INSERTION 3
#define PRINT_ARRAY 4
#define RESET 5
#define STOP 6

/*******define tableau********/
#define MAX_SIZE_ARRAY 20 //taille maximum du tableau
#define MAX_VAL 1000      //Valeur maximale � rentrer � chaque case
#define MIN_VAL -1000     //Valeur minimale � rentrer � chaque case
#define COMMAND_MIN 1     //Commande minimale pour la gestion des switchs
#define COMMAND_MAX 4     //Commande maximale pour la gestion des switchs


/*******Define pour s�lection switch (search)*******/
#define LINEAR_SEARCH 1
#define LINEAR_SENTINEL_SEARCH 2
#define RECURSIVE_SEARCH 3
#define DICHOTOMIC_SEARCH 4

/*******Define pour s�lection switch (tri)*******/
#define BUBBLE_SORT 1
#define INSERTION_SORT 2

/*********** define pour definir ce qu'on doit afficher pour la fonction print *****/
#define NORMAL 0
#define SORTED_ARRAY 1
#define AFTER_INSERT 2

struct main{
    int cmd;
    unsigned int size_array;
};

/********* prototypes ***************/
    void sort_function(int choice, int size_array, int array[], int command, bool *sorted, bool is_inserted);
    void search_functions(int size_array, int array[], int command);
/** fonctions de recherche **/
    void linear_search(int size_array, int array[], int VAL);
    void linear_sentinel_search(int size_array, int array[], int VAL);
    void dichotomic_search(int size_array, int array[], int VAL);
    int  recursive_linear_search(int size_array, int array[], int i,  int VAL);
/** fonctions de tri **/
    void bubble_sort(int size_array, int array[], int* command);
    void insertion_sort(int size_array, int array[], int*command, bool insertion);
/** fonctions li�es au tavleau **/
    void print_array(int size_array, int array[], int specific_print);
    int enter_the_value_to_find_in_the_array();
    void fill_array(int size_array, int array[]);
    int enter_size_array();
    int insertion_element(int size_array, int array[], int command, bool *insertion);
/** deux sous switch qui r�pondent au switch menu **/
    int sort_methode_choice();
    int search_method_choice();

/** affiche le menu **/
    int main_menu();

void reset();
